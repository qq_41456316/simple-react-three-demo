# simple-react-three-demo



![](pics/1.png)

Simple React Three Demo 是一个演示如何使用 React 和 Three.js 加载和显示 glTF 模型的示例项目。

## 特点

- 通过 React 和 Three.js 构建，简洁易懂。
- 支持加载和显示 glTF 格式的 3D 模型。
- 包含简单易用的示例代码，适合学习和快速上手。

## 用法

要安装并运行此项目，首先确保你已经安装了 [Node.js](https://nodejs.org/) 和 [npm](https://www.npmjs.com/)。

然后在终端中执行以下命令：

```bash
git clone https://gitcode.com/qq_41456316/simple-react-three-demo.git
cd simple-react-three-demo
npm install
npm start
```

## 贡献

欢迎贡献代码，提出问题或建议。